## Technologies


Python

Flask

PostgreSQL

Heroku

REST API


## SetUp & Run

initialize database:

$ heroku run python -a flask-api-prod

.>>> from app import db

.>>> db.create_all()

.>>> exit()

$ curl https://flask-psql-staging.herokuapp.com/api/user

$ curl -H 'Content-Type: application/json' -XPOST https://flask-psql-staging.herokuapp.com/api/user -d '{"username": "tejesh", "password": "passwd", "email": "fd@trg.com", "firstname": "tej", "lastname": "esh"}'

$ curl -H 'Content-Type: application/json' -XGET https://flask-psql-staging.herokuapp.com/api/user

$ curl -H 'Content-Type: application/json' -XPUT https://flask-psql-staging.herokuapp.com/api/user/1 -d '{"username": "tejagr", "email": "tej@agr.com"}'

$ curl -H 'Content-Type: application/json' -XGET https://flask-psql-staging.herokuapp.com/api/user/1

$ curl -H 'Content-Type: application/json' -XDELETE https://flask-psql-staging.herokuapp.com/api/user/1


$ heroku ps:scale web=1 -a flask-api-staging

Scaling dynos... done, now running web at 1:Free